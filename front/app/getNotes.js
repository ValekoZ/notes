var url = "https://api.valekoz.fr/"

var xmlHttp = new XMLHttpRequest();
xmlHttp.open("GET", url+"note", false)
xmlHttp.send(null)
response = JSON.parse(xmlHttp.responseText)

if (response.status == "success"){
    notes = response.content;

    accordion = document.getElementById("accordionNotes");

    content = accordion.innerHTML;

    md = new Remarkable();

    notes.forEach(note => {
        index = notes.indexOf(note)
        content += '<div class="accordion-item">\n';
        content += '<h2 class="accordion-header" id="heading' + index +'">\n';
        content += '<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse' + index + '" aria-expanded="false" aria-controls="collapse' + index + '">';
        content += note.title;
        content += '</button>\n';
        content += '</h2>\n';
        content += '<div id="collapse' + index + '" class="accordion-collapse collapse" aria-labelledby="heading' + index + '" data-bs-parent="#accordionNotes">';
        content += '<div class="accordion-body">'
        content += md.render(note.content);
        content += '</div>';
        content += '</div>';
        content += '</div>';
    });

    accordion.innerHTML = content;
}