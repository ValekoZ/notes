var url = "https://api.valekoz.fr/";

function createNote(){
    var title = document.getElementById("title").value;
    var content = document.getElementById("content").value;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", url + "create/", false);
    xmlHttp.send(JSON.stringify({"title": title, "content": content}));
    document.location = "index.html";
}