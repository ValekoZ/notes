"""
Rest API pour l'application de notes
"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional
from pydantic import BaseModel

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Note(BaseModel):
    title: str
    content: str

notes = []


@app.get("/note/")
async def get_note(id: Optional[int] = None):               # pylint: disable=unsubscriptable-object
    """
    Liste toutes les notes si id n'est pas renseigné, donne les infos sur la note d'indice `id` sinon
    """

    if id is None:
        return {"status": "success", "content": notes}
    if 0 <= id < len(notes):
        return {"status": "success", "content": notes[id]}
    return {"status": "failed"}

def parse_note(note: Note):
    note.title   = note.title  .replace("<", "&lt;").replace(">", "&gt;")
    note.content = note.content.replace("<", "&lt;").replace(">", "&gt;")
    return note

@app.post("/create/")
async def create_note(note: Note):
    """
    Ajoute une note à la liste
    """

    notes.append(parse_note(note))

    return {"status": "success", "id": len(notes)-1}

@app.post("/edit/")
async def edit_note(id:int, note: Note):
    """
    Permet d'éditer une note existante
    """

    if 0 <= id < len(notes):
        notes[id] = parse_note(note)

        return {"status": "success", "id": id}
    return {"status": "failed"}


note = Note(title="Lorem ipsum", content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
notes.append(parse_note(note))

note = Note(title="Test XSS", content="<script>alert('XSS')</script>;")
notes.append(parse_note(note))

note = Note(title="Test markdown", content="# Test markdown\n\n- Test 1\n- Test 2\n\nTexte en **gras** et en *italique*\n\n```python\nprint('Hello World !')\n```")
notes.append(parse_note(note))